#/usr/bin/bash

groupadd group_a
groupadd group_b

useradd admin_test

useradd lambda_a group_a
useradd lambda_b group_b

mkdir dir_a
mkdir dir_b
mkdir dir_c

chgrp group_a ./dir_a
chgrp group_b ./dir_b

 chmod 775 dir_a
 chmod 775 dir_b
chmod 755 dir_c

chmod g+s dir_a
chmod g+s dir_b

chmod +t dir_a
chmod +t dir_b

