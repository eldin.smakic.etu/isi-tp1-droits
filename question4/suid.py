#!/usr/bin/python3.8

import os 
  
# Get the effective user ID 
# of the current process 
# using os.geteuid() method 
euid = os.geteuid() 
egid = os.getegid()

print(f'EUID {euid} and EGID {egid}')