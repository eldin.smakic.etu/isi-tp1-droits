# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: SMAKIC Eldin: eldin.smakic.etu@univ-lille.fr

- Nom, Prénom, email: ___

## Question 1

Oui il peut écrire car le groupe a les droits d'écriture sur le fichier

## Question 2

Le caractère x pour un répertoire signifie qu'il peut être ouvert, cela nous permet d'acceder aux fichiers/dossier contenue dedans, bien sur pour les lister il nous faudra la permition de lecture sinon il faudra connaitre le chemin des fichiers/dossiers.

En enlevant ce droit nous pouvons quand même y entrer cas nous somme en même temps le créateur du dossier qui a toujours les droits d'éxecution

avec la création du fichier data, on essayant avec `toto`
la commande ```ls -al mydir```
le systeme nous indique que nous n'avons pas la permision mais nous montre quand même les noms des fichiers contenu dans `mydir`

## Question 3

RUID 1001 , RGID 1001, EUID 1001, EGID 1001 et le processus arrive à ouvrir le fichier car nous avons simplement enlevé l'execution mais comme nous savons le chemin exacte cela ne pose pas problème.

RUID 1001 , RGID 1001, EUID 1000, EGID 1001, on voit ici, que l'EUID est differennt et correspond à celui de `ubuntu` et il arrive à l'ouvrir

## Question 4

Les valeurs sont les même celui de toto (1001 et 1001)

l'utilisateur va utiliser une commande (qui change son password) avec un set-user-id activé pour pouvoir modifier le fichier /etc/passwd

## Question 5

La commande chfn permet de changer les informations d'un utilisateurs (prénom, nom etc)

seul le root à les droits d'écritures mais il possède le set-user-id ce qui permet à n'importe quelle utilisateur de modifier ces informations

Les infos on bien étaient mise à jour dans le fichier passwd

## Question 6

Les mots de passe sont stocké dans le fichier /etc/shadow qui lisible et écrivable que par le root, ce qui permet d'éviter que tout les utilisateurs on accès au  mot de passe hashé de chaque utilisateur, même en ayant que le hash en peut chercher à deviner le mot de passe et savoir si des utilisateurs on les même mot de passe.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








