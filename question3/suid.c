#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    FILE *f;
    char caractereActuel;
    int comp;
    printf("RUID %d , RGID %d, EUID %d, EGID %d \n ",getuid(), getgid(),geteuid(),getegid());
    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File open correctly \n");
     do {
            comp = fgetc(f); 
            caractereActuel = comp;
            printf("%c", caractereActuel); 
        } while (comp != EOF); 
    fclose(f);
    return 0;
}
